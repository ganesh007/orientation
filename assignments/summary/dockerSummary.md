

# DOCKER SUMMARY 

* Docker is a platform for developing,running applications etc.
* It gives a seprate infrastructure to our applicationsso as to deliver our project quickly
* Using docker our infrastructure can be managed in the same ways our applications are managed. 
* One major advantage is the reduction in delay between between prototyping and production.

#### Docker service providers
* There are many companies providing docker services few of the well known ones are
  * Amazon web service
  * Microsoft Azure
  * Digital ocean 

#### Docker platform
* Develop your application and its supporting components using containers.
* The container becomes the unit for distributing and testing your application.

#### Docker architecture 

*The docker architecture is illustarted in the figure below

![photo](https://devopedia.org/images/article/101/8323.1565281088.png)

#### Docker Workflow

* The Workflow adopted by comapnies while working with docker is illustarted below.
![photo](https://collabnix.com/wp-content/uploads/2015/10/WithoutDocker_workflow.jpg)

### Docker Terms

![DockerTerminology](https://image.slidesharecdn.com/dockerve-140813074209-phpapp01/95/virtual-container-docker-21-638.jpg?cb=1418972993)


##### Docker Image

* When we install OS for VM, we install its image(.iso file). Just like that Docker has image.
* It contains everything to develop and run an application :
  * code
  * runtime
  * libraries
  * environment variables
  * configuration files

##### Container 

* It is just an running Docker image.

##### Docker Hub

* Docker Hub is like GitHub but for docker images and containers.

##### Docker Daemon

* Docker daemon runs on host system. The users cannot interact directly with Docker daemon but only through Docker clients.

##### Docker Client

* Docker Client is the chief user interfacing for Docker and it is in docker binary format. Docker daemon will accept the docker commands from users and establishes to and fro communication with Docker daemon.

##### Docker Swarm

Docker Swarm is domestic cluster for Docker. This will allow creation and accessing to a collection of Docker hosts with the help of Docker tools. As Docker Swarm acts as worthful API for Docker, any of Docker tools which are communicating with Docker daemon could use Swarm for transparently scaling different hosts.
 


## Docker Commands

![dockercommnds](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2016/12/Docker-Commands-Docker-Interview-Questions-Edureka-1.png)

* Runs a command in a new container.
```bash 
  $ docker run
```

* Starts one or more stopped containers
```bash 
  $ docker start
```

* docker stop – Stops one or more running containers
```bash 
  $ docker stop
```

 
* Builds an image form a Docker file
```bash 
  $ docker build
```


* Pulls an image or a repository from a registry
```bash 
  $ docker pull
```


* Pushes an image or a repository to a registry
```bash 
  $ docker push
```

* Exports a container’s filesystem as a tar archive
```bash 
  $ docker export
```


* Runs a command in a run-time container
```bash 
  $ docker exec
```

