# Git Basics

## What is Git?

* Git is a command line program which allows you to track versions of code of any text documents.
* Git is used by teams with diffrent departments to coordinate with each other
* It helps in diffrent ways 
   * It keeps a track of all the previous versions of code
   * It allows the teams to keep a track of each department
   * Keeps a track of all the changes made however small they are 
   * It does all this by organising files into a repository(folder)  


### Workflow of Git

![photo](https://support.cades.ornl.gov/user-documentation/_book/contributing/screenshots/git-workflow-steps.png)
   

* Git has remote version where teams can collaborate remotely these services are offered by github,gitlab etc.

### Workflow of a remote version of Git


![photo](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)

## Important Terminology

### Repository(-repo)

Repository is a collection of files and folders that you’re using git to track.

### GitLab

The 2nd most popular remote storage solution for git repos. 1st being GitHub.

### Commit

Think of this as saving your work. When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment. The commit will only exist on your local machine until it is pushed to a remote repository.

### Push 

Pushing is essentially syncing your commits to the repository.

### Branch

You can think of your git repo as a tree. The trunk of the tree, the main software, is called the "Master Branch". The branches of that tree are, well, called branches. These are separate instances of the code that is different from the main codebase.

### Merge

When a branch is free of bugs (as far as you can tell, at least), and ready to become part of the primary codebase, it can be merged into the master branch. Merging is just what it sounds like: integrating two branches together.

### Clone

Cloning a repo is pretty much exactly what it sounds like. It takes the entire online repository and "makes an exact copy" of it on your local machine.

### Fork

Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.


## SOME GIT COMMANDS


![gitcommnads](https://res.cloudinary.com/practicaldev/image/fetch/s--ShHSfi-a--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://cl.ly/1N2U2i2Z2C16/Image%25202018-04-11%2520at%252012.47.23%2520PM.png)

#### git config

Example:
```bash 
$ git config –global user.name “[name]”
$ git config –global user.email “[email address]”
```
#### git init

Example:
```bash 
$ git init [repository name]
```
#### git clone

Example:
```bash 
$ git clone [url]
```
#### git add

Example:
```bash 
$ git add [file]
```

#### git commit

Example:
```bash 
$ git commit -m “[ Type in the commit message]”
```  




